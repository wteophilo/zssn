class Item < ApplicationRecord
	has_many :inventory
	has_many :suvivors, through: :inventory

	validates :name, :quantity, presence: true

	before_save :default_values
	after_initialize :default_values

	private

	def default_values
		self.name ||= ""
		case self.name.upcase
		when "WATER"
			self.points ||= 4
		when "FOOD"
			self.points ||= 3
		when "MEDICATION"
			self.points ||= 2
		when "AMMUNITION"
			self.points ||= 1
		else
			self.points ||= 0
		end	
	end
end
