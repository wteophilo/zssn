class Suvivor < ApplicationRecord
	has_many :inventory
	has_many :items, through: :inventory

	accepts_nested_attributes_for :items

  	validates :name, :age, :gender, presence: true
  	validates :gender, inclusion: { in: %w{male female} }
  	validates :age, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  	before_save :default_values

  	def add_report(suvivor)
  		suvivor.total_report += 1
		suvivor.infected = true if suvivor.total_report >= 3
		suvivor
	end

	def default_values
		self.total_report ||= 0
		self.infected ||= false
	end

	def has_item?(inventories)
		self.items.each do |item_suvivor|
			inventories.each do |item_procurado|
				have = item_suvivor.name == item_procurado.name
				break if have == false 
			end
		end
	end

	def get_total_of_points(items)
		total = 0
		items.each do |item|
			total += item.quantity * item.points
		end
		total
	end
end
