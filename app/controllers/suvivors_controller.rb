class SuvivorsController < ApplicationController
	before_action :set_suvivor, only: [:show,:edit,:update,:destroy,:report_infection]
	
	def index
		@suvivors = Suvivor.all
		render json: @suvivors, status: :ok
	end

	def new
		@suvivor = Suvivor.new
	end

	def report_infection
		if !@suvivor.nil? 
			@suvivor = @suvivor.add_report(@suvivor)
			if @suvivor.update_columns(total_report: @suvivor.total_report, infected: @suvivor.infected)
				render json: @suvivors, status: :ok
			else
				render json: @suvivors, status: :not_acceptable
			end
		else
			render json: @suvivors, status: :not_found
		end
	end

	def create
		@suvivor = Suvivor.new(suvivor_params)	
		@suvivor.items << set_item
		if @suvivor.save
			render json: @suvivor, status: :created
		else
			render json: nil ,status: :not_acceptable
		end
	end

	def show
		if @suvivor.nil?
			render json: @suvivor, status: :not_found
		else
			render json: @suvivor, status: :ok
		end 
	end
	
	def edit
	end

	def update
		if @suvivor.update suvivor_params
			render json: @suvivor, status: :ok
		else
			render json: @suvivor, status: :not_acceptable
		end
	end

	def destroy
	end


	private
		def set_suvivor
			@suvivor = Suvivor.find(params[:id]) rescue nil
		end

		def suvivor_params
			 params.permit(:name,:age,:gender,:lon,:lat,:items=>[:name,:quantity])
		end

		def set_item
			items = params[:items]
			if !items.empty? 
				items = items.split(",")
				items.map{|i|
					array = i.split(":")
					Item.new name: array[0], quantity:array[1]
				}
			end
		end		
end
