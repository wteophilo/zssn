class ReportController < ApplicationController
	before_action :total_suvivors,only:[:percentage_infected,:percentage_non_infected]
	before_action :total_suvivor_infected,only:[:percentage_infected]
	before_action :total_suvivor_non_infected,only:[:percentage_non_infected]

	def percentage_infected
		render json: compute_percent(@total_suvivor_infected.size), status: :ok
	end

	def percentage_non_infected
		render json: compute_percent(@total_suvivor_non_infected.size), status: :ok
	end

	def lost_points
		total_suvivor_infected = Suvivor.where(:infected => true)
		total = compute_lost_point(total_suvivor_infected)
		render json: total, status: :ok
	end

	private

	def total_suvivors
		@total_suvivors = Suvivor.count 
	end

	def total_suvivor_infected
		@total_suvivor_infected = Suvivor.where(:infected => true)
	end

	def total_suvivor_non_infected
		@total_suvivor_non_infected = Suvivor.where(:infected => false)
	end

	def compute_percent(value)
		if @total_suvivors != 0
			(value * 100) / @total_suvivors
		else
			0
		end
	end

	def compute_lost_point(suvivors)
		@points = 0
		suvivors.each do |suvivor|
			items = Item.joins(:inventory).where("inventories.suvivor_id = #{suvivor.id}")
			items.map{|item| @points += item.points * item.quantity}
		end
		@points
	end
end
