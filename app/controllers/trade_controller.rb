class TradeController < ApplicationController
	before_action :set_suvivor, only: [:items_suvivor]
	before_action :set_suvivor_trade,only: [:trade_item]
	before_action :set_items,only:[:trade_item]

	def items_suvivor
		render json: @suvivor, status: :ok
	end

	def trade_item
		if has_items? && compare_points?
			make_trade(@item_consumer,@suv_request)
			make_trade(@item_request,@suv_consumer)
			destroy(@item_consumer,@suv_consumer)
			destroy(@item_request,@suv_request)
			render json: nil, status: :ok
		else
			render json: nil, status: :not_acceptable
		end
	end

	private

	def make_trade(item_suv,suv)
		item_suv.each do |item_consumer|
			item_request = suv.items.find_by(name: item_consumer.name)
			if !item_request.nil?
				item_request.quantity += item_consumer.quantity
				item_request.save
			else
				suv.items << item_consumer
				suv.save
			end
		end
	end

	def destroy(item_suv,suv)
		item_suv.each do |item_consumer|
			item_request = suv.items.find_by(name: item_consumer.name)
			if !item_request.nil?
				item_request.quantity -= item_consumer.quantity
				item_request.save
			end
		end
	end

	
	def set_suvivor
		@suvivor = Suvivor.find(params[:suvivor_id]) rescue nil
	end

	def compare_points?
		@suv_request.get_total_of_points(@item_request) == @suv_consumer.get_total_of_points(@item_consumer)
	end

	def has_items?
		@suv_request.has_item?(@item_request) && @suv_consumer.has_item?(@item_consumer)
	end

	def set_suvivor_trade
		@suv_request = Suvivor.find(params[:suvivor_id]) 
		@suv_consumer = Suvivor.find(params[:consumer_id])
	end

	def set_items
		@item_request = set_item(params[:suvivor_item])
		@item_consumer = set_item(params[:consumer_items])	
	end

	def set_item(items)
		if !items.empty? 
			items = items.split("|")
			items.map{|i|
				array = i.split(":")
				Item.new name: array[0], quantity:array[1]
			}
		end
	end	
end
