class SuvivorSerializer < ActiveModel::Serializer
	attributes :id,:name,:age,:gender,:infected
	has_many :items

	class ItemSerializer < ActiveModel::Serializer
		attributes :name,:quantity,:points
	end
end