Rails.application.routes.draw do
	resources :suvivors
	post 'suvivors/report_infection/:id', to: 'suvivors#report_infection'

	get 'reports/percentage_infected',to: 'report#percentage_infected'
    get 'reports/percentage_non_infected',to: 'report#percentage_non_infected'
    get 'reports/lost_points',to: 'report#lost_points'


    get 'properties/:id', to: 'trade#items_suvivor'
    post 'properties/trade_item/:suvivor_id&:suvivor_item&:consumer_id&:consumer_items', to: 'trade#trade_item'
end
