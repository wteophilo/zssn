class CreateInventories < ActiveRecord::Migration[5.1]
  def self.up
    create_table :inventories do |t|
      t.references :suvivor,:item
      t.timestamps
    end
  end

  def self.down
  	drop_table :inventories
  end
end
