class CreateSuvivors < ActiveRecord::Migration[5.1]
  def change
    create_table :suvivors do |t|
      t.string :name
      t.integer :age
      t.string :gender
      t.boolean :infected
      t.integer :total_report
      t.string :lon
      t.string :lat

      t.timestamps
    end
  end
end
