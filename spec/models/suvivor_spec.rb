require "rails_helper"

RSpec.describe Item, type: :model do
	context "Validate Field Suvivors:" do
		it "Create a Suvivor with all fields fill" do
			@suvivor = FactoryBot.build(:suvivor)
			expect(@suvivor.valid?).to be_truthy
		end

		it "Create a Suvivor without a name" do
			@suvivor = FactoryBot.build(:suvivor)
			@suvivor.name = ""
			expect(@suvivor.valid?).to be_falsey
		end
	end

	context "Inventory" do
		let(:suvivor){FactoryBot.create :suvivor}
		let(:items){FactoryBot.create_list(:item,2)}		
		let(:item){FactoryBot.create :item}
		it "Verify if has items on inventory" do
			suvivor.items << item
			suvivor.has_item?(items)	
		end

		it "Verify total of points" do
			expect(suvivor.get_total_of_points(items)).to eq(16)
		end

	end
end

