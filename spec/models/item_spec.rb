require 'rails_helper'

RSpec.describe Item, type: :model do
	let(:item){FactoryBot.create :item}
	it "Create a Item with all fields fill" do
		expect(item.valid?).to be_truthy
	end

	it "Create a Item without a name" do
		item.name = ""
		expect(item.valid?).to be_falsey
	end
end
