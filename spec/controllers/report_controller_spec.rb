require 'rails_helper'
require 'pry'

RSpec.describe ReportController, type: :controller do
	context "Percentage infected" do
		let(:suvivor){FactoryBot.create :suvivor}
		it "Should return status 200" do
			get :percentage_infected
			expect(response).to have_http_status(:ok)
			expect(response.content_type).to eql "application/json"
		end
	end

	context "Percentage non infected" do
		let(:suvivor){FactoryBot.create :suvivor}
		it "Should return status 200" do
			get :percentage_non_infected
			expect(response).to have_http_status(:ok)
			expect(response.content_type).to eql "application/json"
		end
	end

	context "Lost Points" do
		let(:suvivor){FactoryBot.create :suvivor}
		it "Should return status 200" do
			get :lost_points
			expect(response).to have_http_status(:ok)
			expect(response.content_type).to eql "application/json"
		end
	end

end
