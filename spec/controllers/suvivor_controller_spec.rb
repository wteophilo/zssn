require 'rails_helper'
require 'pry'

RSpec.describe SuvivorsController, type: :controller do
	context "Index" do
		it "should return status 200" do
			get :index
			expect(response).to have_http_status(:ok)
		end
	end

	context "Create a suvivor" do
		it "Should return status 201" do
			post :create, params: {:name => "Himura Kenshin",
										:age => 28,
				                        :gender => "male",
				                        :lon => "000987",
				                        :lat => "987000",
				                    	:items => "[Food:3,Medication:1,Ammunition:5]"}
			expect(response).to have_http_status(:created)
			expect(response.content_type).to eql "application/json"
		end

		it "should return status 406" do
			post :create, params: {:name => " ",
										:age => 28,
				                        :gender => "male",
				                        :lon => "000987",
				                        :lat => "987000",
				                    	:items => "[Food:3,Medication:1,Ammunition:5]"}
			expect(response).to have_http_status(:not_acceptable)
			expect(response.content_type).to eql "application/json"
		end
	end

	context "Return a single suvivor" do
		let(:suvivor){FactoryBot.create :suvivor}
		it "Should return status 200" do
			get :show, params:{:id => suvivor.id}
			expect(response).to have_http_status(:ok)
			expect(response.content_type).to eql "application/json"
		end
		
		it "Should return  status 404" do
			get :show, params: {:id => suvivor.id + 4}
			expect(response).to have_http_status(:not_found)
			expect(response.content_type).to eql "application/json"
		end

	end

	context "Report a Suvivor" do
		let(:suvivor){FactoryBot.create :suvivor}
		it "Should return status 200" do
			post :report_infection, params:{:id => suvivor.id}
			expect(response).to have_http_status(:ok)
			expect(response.content_type).to eql "application/json"
		end

		it "Should return  status 404" do
			post :report_infection, params: {:id => 9999}
			expect(response).to have_http_status(:not_found)
			expect(response.content_type).to eql "application/json"

		end
	end


end
