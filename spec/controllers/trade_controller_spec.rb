require 'rails_helper'

RSpec.describe TradeController, type: :controller do
	context "Items from a single suvivor" do
		let(:suvivor){FactoryBot.create :suvivor}
		it "Should return status 200" do
			get :items_suvivor, params:{:id => suvivor.id}
			expect(response).to have_http_status(:ok)
			expect(response.content_type).to eql "application/json"
		end
	end

# post 'properties/trade_item/:suvivor_id&:suvivor_item&:consumer_id&:consumer_items', to: 'trade#trade_item'

	context "Make trade" do
		let(:suvivor){FactoryBot.create :suvivor}
		let(:suvivor2){FactoryBot.create :suvivor}
		let(:item){FactoryBot.create :item}


		it "Should return status 200" do
			suvivor.items << item
			suvivor2.id = 2
			suvivor2.items << item	
		
			post :trade_item, params: {
				:suvivor_id => suvivor.id,
				:suvivor_item => "Water:1",
				:consumer_id => suvivor2.id,
				:consumer_items => "Water:1"
			}

			expect(response).to have_http_status(:ok)
			expect(response.content_type).to eql "application/json"
		end

		it "Should return status 406" do		
			suvivor.items << item
			suvivor2.id = 2
			suvivor2.items << item	
			post :trade_item, params: {:suvivor_id => suvivor.id,
										:suvivor_item => "Water:10",
										:consumer_id => suvivor2.id,
										:consumer_items => "Medication:2"}
			expect(response).to have_http_status(:not_acceptable)
			expect(response.content_type).to eql "application/json"
		end
	end
end
