# ZSSN (Zombie Survival Social Network)

The world as we know it has fallen into an apocalyptic scenario. A laboratory-made virus is transforming human beings and animals into zombies, hungry for fresh flesh.
You, as a zombie resistance member (and the last survivor who knows how to code), was designated to develop a system to share resources between non-infected humans.


# API

##SUVIVORS

url: /suvivors
description: List of all Suvivors
method: get
Response:
* Http Status Code: 
	* Sucess: 200
* List Suvivors

url: /suvivors/:id
description: Single Suvivors
method: get
Response:
* Http Status Code: 
	* Sucess: 200
	* Fail: 404
* Suvivor

url: /suvivors/?lon=?&lat
description: Update longitude and latitude
method: patch
Response:
* Http Status Code: 
	* Sucess: 200
	* Fail: 406
* Suvivor

url: /suvivors/?name=?&age=?&gender=?&lon=?&lat=?&items=?
description: Create a new Suvivor. To add the items the pattern use is name:quantity for exemplo "Water:2,Food:2"
method: post
Response:
* Http Status Code:
	* Sucess: 201
	* Fail: 406
* Suvivor

url: /suvivors/report_infection/:id'
description: Report a suvivor infected
method: post
Response:
* Http Status Code: 
	* Sucess: 200
	* Fail: 404

##Report

url: reports/percentage_infected
description: Percentage of infected survivors
method: get
Response:
* Http Status Code: 
	* Sucess: 200
* Total of people infected

url: reports/percentage_non_infected
description: Percentage of infected non survivors
method: get
Response:
* Http Status Code: 
	* Sucess: 200
* Total of people non infected


url: reports/lost_points
description: Total of points losted
method: get
Response:
* Http Status Code: 
	* Sucess: 200
* Total of points losted

##Properties

url: properties/:suvivor_id
description: Show the invetory of a sigle suvivor
method: get
Response:
* Http Status Code: 
	* Sucess: 200
* List of itens


url: 'properties/trade_item/:suvivor_id&:suvivor_item&:consumer_id&:consumer_items
description: Make de trade between suvivors
method: post
Reponse:
* Http Status Code:
	* Sucess: 200
	* Fail: 406